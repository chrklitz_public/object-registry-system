# Object-Registry-System

## Concept
C++ objects like plugins can register themselve to any dedicated registry-class-instance by selecting the type of the registry with a 
template parameter. They are then forwarded to the registry object which enforces a required plugin-interface. The registries instances 
are managed by an object called RegistryManager. This enables the plugin the statically select registries to register in. Furthermore
the application can decide which registries to use and add to the registry-manager. 
This can be useful for things like GUIs or other systems which support multiple plugin-types. For example a GUI could provide an
interface for adding menu-entries, different previews or dialogues. With the object-registry-system the objects or plugins can decide by
themselve which registry to use and they can even register to multiple ones. The registries which provide plugin/object-access to different
parts of an application can be added on top of existing architectures.

## Components
class *RegistryManager* (library/registrymanager.h) - handles instances of base-type Registry  
class *Registry* (library/registry.h) - Baseclass for registries: This class does not contain any virtual functions since it is only used to store registries in the RegistryManager  

custom Registry - A registry class which is defined in the application needs to satisfy the following conditions:  
1) contain a function with the signature:  

     ```c++
     template<typename PluginType>  
     void register_object(PluginType *p);
     ```

     This function is called with the registering object of type PluginType as parameter. This allows the registry to do a compile-time check 
     on the PluginType object to enforces things like required functions. You can further make use of lambda-functions, Qt-signals or other things
     to store the access to the registered object for later use if needed. This static check is also possible for plugins because the "real" 
     version with inserted proper PluginType is compiled together with the plugin.  
  
2) Use macro ADD_REGISTRY(\<class-name\>) or ADD_REGISTRY2(\<class-name\>, \<custom-name\>) somewhere outside of the class outside of any namespace.  
The macro allows the registry-manager to determine the registry-instance from the by the registering object selected type.
It essentially enables type-to-object mapping. However this comes with the risk of loosing a bit of type-safety but hey...
we are talking about C++.  

## Example usage
Note that whenever the word plugin is used, really a class which can be instantiated and which also could live in any other context is meant, 
therefore it does not has to be a pluging or being loaded at runtime.

Plugin A wants to register itself into the registries MenuEntryReg and CustomDialogueReg.
To do this, the application it is loaded into, needs to support plugin-registration as defined in this repository. Furthermore class A
needs to contain the public non-template function below:

```c++
class A {
    ...
public:
    void register_this(RegistryManager &regManager) {
        regManager.register_object<MenuEntryReg>(this);
        regManager.register_object<CustomDialogueReg>(this);
    }
}
```

Inside the application the classes MenuEntryReg and CustomDialogueReg needs to be defined and also be available for the plugin A (e.g. headers).
The registry MenuEntryReg could look like the following (CustomDialogueReg analogue):

```c++
class MenuEntryReg {
public:
    template <typename PluginType>
    void register_object(PluginType *p) {
        gui->add_menuentry(p->create_menuentry());
    }
}
ADD_REGISTRY(MenuEntryReg) // Alternatively: ADD_REGISTRY2(MenuEntryReg, "user_xy_lib_z_MenuEntryReg")
```

The regManager parameter is provided by the application and contains a set of registries supported by the application. Inside the application
the object A is created in some way (for example loaded as a plugin) and then A::register_this is called together with a previously 
created instance of the RegistryManager class which needs to already contain the supported registries. Note that the function A::register_this
does not need to look like this, because this function name is not enforced anywhere but for compatibility reasons this convention should be used.

```c++
RegistryManager regManager;

MenuEntryReg menuEntryReg;
// ... some more registries here
CustomDialogueReg cusDiaReg;


manager.add_registry(menuEntryReg);
manager.add_registry(cusDiaReg);

...

// load plugin A and register it: 
Plugin *p = ...;
// p is of type A. However since it is loaded at runtime, its detailed type is unknown for the application. It is probably good for the 
// Plugin super-class to enforce the register_this function as a virtual function.

// p can now be registered with the regManager instance from above:
p->register_this(regManager);
```

This results results in the plugin p (of hidden type A) to register itself into its selected registry objects menuEntryReg and cusDiaReg.

## LICENSE
Look at the LICENSE file if existing. With regard to the LICENSE feel free to use this library and change files if allowed but it would be 
nice if you leave the comments at the top of the files.
