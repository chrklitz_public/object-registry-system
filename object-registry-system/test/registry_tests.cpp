#include <object-registry-system/registry.hpp>
#include <object-registry-system/registrymanager.hpp>

static const std::string _testmessage = "test-message";

class MyReg : public Registry
{
public:
  template<typename T>
  void register_object(T &obj)
  {
    registry_message = obj.testmessage();
  }

  std::string registry_message = "";
};
ADD_REGISTRY(MyReg)

class MyAlternativeReg : public Registry
{
public:
  template<typename T>
  void register_object(T &object)
  {
    (void) object;
  }
};
ADD_REGISTRY2(MyAlternativeReg, "ors::test::MyAltPlugin")

class MyPlugin
{
public:
  bool register_this(RegistryManager &manager) {
    return manager.register_object<MyReg>(this);
  }

  std::string testmessage() { return _testmessage; }
};

class MyAlternativePlugin
{
public:
  bool register_this(RegistryManager &manager)
  {
    return manager.register_object<MyAlternativeReg>(this);
  }
};

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

BOOST_AUTO_TEST_CASE( registry_test )
{
  MyReg reg;
  RegistryManager manager; // Alternatively add registries as constructor arguments
  BOOST_CHECK_MESSAGE(manager.add_registry(reg),
                      "the manager-object does not already contain a registry"
                      "of this type which means that it should be add-able");

  BOOST_CHECK_MESSAGE(reg.registry_message != _testmessage,
                      "registry object 'reg' should not have registered"
                      "an object of type MyPlugin yet");

  MyPlugin p; // can be loaded dynamically at runtime
  BOOST_CHECK_MESSAGE(p.register_this(manager),
                      "registration should be successful because the registry-manager"
                      "contains all required registries");
  BOOST_CHECK_MESSAGE(reg.registry_message == _testmessage,
                      "registry object 'reg' should have registered"
                      "an object of type MyPlugin");


  MyReg reg2;
  BOOST_CHECK_MESSAGE(!manager.add_registry(reg2),
                      "the manager-object already contains a registry of this type"
                      "which means that the addition should fail and return false");

  MyAlternativePlugin pAlt;
  BOOST_CHECK_MESSAGE(!pAlt.register_this(manager),
                      "registration should fail because the registry-manager"
                      "does not contain the correct registry yet");

  MyAlternativeReg regAlt;
  BOOST_CHECK_MESSAGE(manager.add_registry(regAlt),
                      "the manager-object does not already contain a registry"
                      "of this type which means that it should be add-able");
}
