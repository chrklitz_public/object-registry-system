TEMPLATE = app
CONFIG += c++11
CONFIG -= qt
# CONFIG += console
CONFIG -= debug_and_release debug_and_release_target

DEFINES += OBJECT-REGISTRY-SYSTEM_LIB
DEFINES += QT_DEPRECATED_WARNINGS

include(boost_include.pri)


SOURCES += 

HEADERS += \
  object-registry-system/registry.hpp \
  object-registry-system/registrymanager.hpp

test {
  include(test/testsources.pri)
} else {
  SOURCES += object-registry-system/main.cpp
}
