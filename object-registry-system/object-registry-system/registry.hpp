/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/object-registry-system
 * License: See the LICENSE file in the git-repository
 */

#ifndef REGISTRY_H
#define REGISTRY_H


#define ADD_REGISTRY(classType) ADD_REGISTRY2(classType, #classType)
#define ADD_REGISTRY2(classType, classTypeId) \
  template<> \
  constexpr const char *RegistryInfo::getRegistryTypeNameId<classType>() \
  { \
    return classTypeId; \
  }

/*!
 * \brief The Registry class is the base class for all object-registries. When making a
 * new object-registry class, it must derive from this class to provide a base-class for
 * the \ref RegistryManager. Outside of the new object-registry class the macro
 * ADD_REGISTRY(.) or ADD_REGISTRY2(.) must be used to identify the registry-class-type with a
 * dynamically usable format. Furthermore the class must provide a public function with the signature:
 * template<typename T> void register_object(T &obj).
 */
class Registry
{};

/*!
 * \brief The RegistryInfo class provides access to the id of the object-registry class defined
 * by the macro ADD_REGISTRY(.) or ADD_REGISTRY2(.).
 */
class RegistryInfo
{
public:
  template<typename RegistryType>
  constexpr static const char *getRegistryTypeNameId() {
    return "";
  }
};

constexpr bool constexpr_isequal(char const *one, char const *two)
{
    return (*one && *two) ? (*one == *two && constexpr_isequal(one + 1, two + 1)) : (!*one && !*two);
}


#endif // REGISTRY_H
