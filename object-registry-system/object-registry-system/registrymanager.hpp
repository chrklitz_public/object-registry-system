/*
 * Author: Christopher von Klitzing
 * Repository: https://gitlab.com/chrklitz_public/object-registry-system
 * License: See the LICENSE file in the git-repository
 */
#ifndef REGISTRYMANAGER_H
#define REGISTRYMANAGER_H

#include <map>

#include "registry.hpp"


/*!
 * \brief The RegistryManager class manages a customizable set of object-registry classes.
 * This class basically plays the role of a facade which gives unified access to all contained object-registries.
 * When registering an object it can select the registry-object by use of its class-type. This class
 * is and should not be templated so that it can get passed to things like plugins easily.
 * The object registration to the underlying registry-object with the selected type can be
 * templated, which allows the object-registry to make static checks and assertions on the
 * registering object/plugin. Each instance of this class can only hold at max one reference to a
 * registry-object of a certain type. The unique-constrained is checked by its id defined with the
 * ADD_REGISTRY macro.
 */
class RegistryManager
{
public:

  /*!
   * \brief RegistryManager constructs this class with a pre-defined set of object-registries.
   * Note that the correct functionality of this class depends on the deducted type of each registry!
   *
   * \param registries variadic template list of object-registries
   */
  template<typename... RegistryType>
  RegistryManager(RegistryType &... registries)
  {
    addRegistries(registries...);
  }

  RegistryManager(const RegistryManager &other) = delete;
  const RegistryManager &operator=(const RegistryManager &other) = delete;

  /*!
   * \brief this function adds a single registry-object to this class. If there is already
   * a registry of the same type, the new registry is not added!
   *
   * \param reg object-registry instance
   * \return true if the registry was not already in this class, false otherwise
   */
  template<typename RegistryType>
  bool add_registry(RegistryType &reg)
  {
    static_assert (!constexpr_isequal(RegistryInfo::getRegistryTypeNameId<RegistryType>(), ""),
                   "No type-id for the registry defined. Maybe you forgot to use the ADD_REGISTRY macro outside"
                   "of the registry class.");
    static_assert(std::is_base_of<Registry, RegistryType>::value,
                  "Registry object must be of type Registry. The given registry object needs to derive from class Registry.");

    std::string typeNameId = RegistryInfo::getRegistryTypeNameId<RegistryType>();
    if (registryMap.find(typeNameId) == registryMap.end()) {
      registryMap[typeNameId] = &reg;
      return true;
    }
    return false;
  }

  /*!
   * \brief this function can be called to register an object of type T to the object-registry of
   * the specified type. Note that the object-registry can have its own constraints to the registering
   * object which might cause this function to fail already when compiling a call to this function.
   * This function can be called directly by passing a plugin to it and registering it to
   * its selected type but the original concept was to let the plugin select by itself, by passing an
   * instance of this class to a function of the plugin.
   *
   * \param obj object which should be registered by the object-registry
   * \return true, if registration successful
   */
  template<typename RegistryType, typename T>
  bool register_object(T *obj)
  {
    if (obj == nullptr)
      return false;

    static_assert (std::is_base_of<Registry, RegistryType>::value,
                   "The given template-type RegistryType is not convertible into type Registry.");

    std::string typeNameId = RegistryInfo::getRegistryTypeNameId<RegistryType>();
    if (registryMap.find(typeNameId) != registryMap.end()) {
      static_cast<RegistryType *>(registryMap[typeNameId])->register_object(*obj);
      return true;
    }
    // else:
    return false;
  }

private:
  template<typename RegistryType, typename... Args>
  bool addRegistries(RegistryType &reg, Args &... rest)
  {
    if (add_registry(reg)) {
      return addRegistries(rest...);
    }
    return false;
  }
  bool addRegistries() { return true; }

private:
  std::map<std::string, Registry *> registryMap;
};



#endif // REGISTRYMANAGER_H
