#include <iostream>
#include "registrymanager.hpp"

using namespace std;

class MyReg : public Registry
{
public:
  template<typename T>
  void register_object(T &obj) {
    obj.sayHello();
  }
};
ADD_REGISTRY(MyReg)

class MyPlugin
{
public:
  void register_this(RegistryManager &manager) {
    manager.register_object<MyReg>(this);
  }

  void sayHello() {
    std::cout << "Hello from plugin" << std::endl;
  }
};



int main() {
  MyReg reg;
  RegistryManager manager(reg /*, more registries */);
  // Alternative: manager.add_registry(reg);

  MyPlugin p; // can be loaded dynamically at runtime
  p.register_this(manager);

  return 0;
}
